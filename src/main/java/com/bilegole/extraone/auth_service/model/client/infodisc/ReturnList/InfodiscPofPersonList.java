package com.bilegole.extraone.auth_service.model.client.infodisc.ReturnList;

import com.bilegole.extraone.auth_service.model.application.Infodisc.InfodiscPofPersonOrgDetail;

/**
 * 基金从业人员资格注册信息返回值
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/person/personOrgList.html">样本页</a>
 */
public class InfodiscPofPersonList implements returnListItem<InfodiscPofPersonOrgDetail> {
    String exFundManagerNum;
    String extInvestmentManagerNum;
    String extOperNum;
    String extSalesmanNum;
    /**
     * 员工人数
     */
    String extWorkerTotalNum;
    /**
     * 基金经理
     */
    String fundManagerNum;
    /**
     * 投资经理
     */
    String InvestmentManagerNum;
    /**
     * 基金从业资格人数
     */
    String operNum;
    String orgCode;
    /**
     * 机构名称
     */
    String orgName;
    String orgNameChineseSpell;
    /**
     * 机构类型
     */
    String orgType;
    /**
     * 基金销售业务资格
     */
    String salesmanNum;
    String userId;
    String workTotalNum;

    @Override
    public void transform(InfodiscPofPersonOrgDetail infodiscPofPersonOrgDetail) {

    }
}
