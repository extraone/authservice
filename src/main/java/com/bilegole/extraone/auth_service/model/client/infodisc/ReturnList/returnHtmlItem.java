package com.bilegole.extraone.auth_service.model.client.infodisc.ReturnList;

public interface returnHtmlItem<T> {
    void transform(T t);
}
