package com.bilegole.extraone.auth_service.model.application.Infodisc;

import lombok.Data;

import java.util.Date;

/**
 * 人员信息
 */
@Data
public class InfodiscEmployee {
    /**
     * 姓名
     */
    String name;
    /**
     * 性别
     */
    String sex;
    /**
     * 证书编号
     */
    String certificateCode;
    /**
     * 从业资格类别
     */
    String certificateType;
    /**
     * 证书取得日期
     */
    Date certificateFetchDate;
    /**
     * 证书状态变更记录
     */
    Integer certificateChangeStatus;
    /**
     * 诚信记录
     */
    Integer HonestRecord;
}
