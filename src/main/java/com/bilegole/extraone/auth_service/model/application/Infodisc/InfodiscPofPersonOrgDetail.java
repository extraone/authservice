package com.bilegole.extraone.auth_service.model.application.Infodisc;

import lombok.Data;

/**
 * 基金从业人员资格注册信息
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/person/personOrgList.html">基金从业人员资格注册信息JSON页</a>
 * 这个对象主要是对某个机构下属的各种员工做一个数量上的统计
 */
@Data
public class InfodiscPofPersonOrgDetail {
    /**
     * 机构名称
     */
    String orgName;
    /**
     * 机构类型
     */
    String orgType;
    /**
     * 员工人数
     */
    String employeeNum;
    /**
     * 基金从业资格
     */
    String qualifiedNum;
    /**
     * 基金销售业务资格
     */
    String qualifiedSalerNum;
    /**
     * 基金经理
     */
    String fundManagerNum;
    /**
     * 投资经理
     */
    String investmentManagerNum;
}
