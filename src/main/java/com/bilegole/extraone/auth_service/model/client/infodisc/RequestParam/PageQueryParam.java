package com.bilegole.extraone.auth_service.model.client.infodisc.RequestParam;

import lombok.Data;

@Data
public class PageQueryParam {
    protected Integer page;
    protected Integer size;
}
