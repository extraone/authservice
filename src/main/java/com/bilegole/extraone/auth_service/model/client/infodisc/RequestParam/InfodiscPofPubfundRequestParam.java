package com.bilegole.extraone.auth_service.model.client.infodisc.RequestParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 基金公司私募投资基金查询列表请求参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class InfodiscPofPubfundRequestParam extends PageQueryParam {
    String foundDateFrom;
    String foundDateTo;
    String mgrName;
    String productCode;
    String productName;
}
