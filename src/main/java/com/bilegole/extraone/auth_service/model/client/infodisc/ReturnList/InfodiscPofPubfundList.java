package com.bilegole.extraone.auth_service.model.client.infodisc.ReturnList;

import com.bilegole.extraone.auth_service.model.application.Infodisc.InfodiscPofPubfundDetail;
import lombok.Data;

import java.util.Date;

/**
 * 私募基金公示查询列表返回值
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/pubfund/index.html">样本页</a>
 */
@Data
public class InfodiscPofPubfundList implements returnListItem<InfodiscPofPubfundDetail> {
    String foundDate;
    String id;
    String mgrName;
    String productCode;
    String productName;
    String registeredDate;
    String trustee;

    @Override
    public void transform(InfodiscPofPubfundDetail infodiscPofPubfundDetail) {

    }
}
