package com.bilegole.extraone.auth_service.model.client.infodisc.RequestParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 证券公司直投基金页面查询参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class InfodiscAoinProductRequestParam extends PageQueryParam {
    String name;
    String aoinName;
    String code;
    String createDateFrom;
    String createDateTo;
}
