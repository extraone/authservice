package com.bilegole.extraone.auth_service.model.client.infodisc.ReturnList;

import com.bilegole.extraone.auth_service.model.application.Infodisc.InfodiscEmployee;

import java.util.Date;

/**
 * 人员信息返回值
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/person/personList.html">样本页</a>
 */
public class InfodiscEmployeeReturnList implements returnListItem<InfodiscEmployee> {
//    String id;
//    String name;
//    String sex;
//    /**
//     *
//     */
//    String certificateCode;
//    /**
//     * 机构名称
//     */
//    String orgName;
//    /**
//     * 从业资格类型
//     */
//    String certificateType;
//    /**
//     * 证书取得日期
//     */
//    Date certificateDate;
//    /**
//     * 证书状态变更记录
//     */
//    String certificateChangeRecord;
//    /**
//     * 诚信记录
//     */
//    String honestTips;

    String accountId;
    String applyId;
    String applyStatus;
    String bizId;
    /**
     * 证书编号
     */
    String certCode;
    String certEndDate;
    /**
     * 基金从业资格类别
     */
    String certName;
    String certObtainDate;
    String certStatusChangeTimes;
    String certRecordNum;
    String education;
    String educationName;
    String flag;
    String officeState;
    /**
     * 所在的机构的名称
     */
    String orgName;
    String ownOrgName;
    String page;
    String personCertHistoryList;
    String personPhotoBase64;
    String removed;
    /**
     * 从业人员性别
     */
    String sex;
    String status;
    String statusName;
    String userId;
    /**
     * 从业人员姓名
     */
    String userName;

    @Override
    public void transform(InfodiscEmployee infodiscEmployee) {

    }
}
