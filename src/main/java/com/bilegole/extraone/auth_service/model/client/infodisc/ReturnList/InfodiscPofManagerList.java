package com.bilegole.extraone.auth_service.model.client.infodisc.ReturnList;

import com.bilegole.extraone.auth_service.model.application.Infodisc.InfodiscPofManagerDetail;
import lombok.Data;

/**
 * 私募基金管理人分类查询公示页面列表查询返回值。
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/manager/managerList.html">样本页</a>
 */
@Data
public class InfodiscPofManagerList implements returnListItem<InfodiscPofManagerDetail> {
    /**
     * 法定代表人/执行事务合伙人(委派代表)姓名
     */
    String artificialaPersonName;
    /**
     * 成立时间
     */
    String establishDate;
    /**
     *
     */
    String fundCount;
    String fundTypeScaleMap;
    /**
     * 是否有诚信信息
     */
    String hasCreditTips;
    /**
     * 是否有提示信息
     */
    String hasSpecialTips;
    String id;
    String managerHasProduct;
    /**
     * 私募基金管理人名称
     */
    String managerName;
    /**
     * 会员类型
     */
    String memberType;
    String officeAddress;
    /**
     * 办公地
     */
    String officeAdrAgg;
    String officeCity;
    String officeCoordinate;
    String officeProvince;
    String orgForm;
    String paidInCapital;
    /**
     * 机构类型
     */
    String primaryInvestType;
    /**
     * 注册地
     */
    String regAdrAgg;
    String regCoordinate;
    String registerCity;
    String registerDate;
    /**
     * 登记编号
     */
    String registerNo;
    String registerProvince;
    String subscribedCapital;
    String url;

    @Override
    public void transform(InfodiscPofManagerDetail infodiscPofManagerDetail) {

    }
}
