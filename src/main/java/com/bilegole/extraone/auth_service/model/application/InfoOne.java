package com.bilegole.extraone.auth_service.model.application;

import com.bilegole.extraone.auth_service.Dao.Application.InfoOnePO;
import lombok.Data;

@Data
public class InfoOne {
    public String info1;

    public InfoOne(InfoOnePO po){
        info1 = po.getInfo1();
    }
}
