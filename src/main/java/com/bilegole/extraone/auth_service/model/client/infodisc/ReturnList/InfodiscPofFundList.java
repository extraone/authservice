package com.bilegole.extraone.auth_service.model.client.infodisc.ReturnList;

import com.bilegole.extraone.auth_service.model.application.Infodisc.InfodiscPofFundDetail;
import lombok.Data;

/**
 * 对私募基金公示列表请求的返回值。
 * 又被称为私募基金管理人基金产品
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/fund/index.html">样本页</a>
 */
@Data
public class InfodiscPofFundList implements returnListItem<InfodiscPofFundDetail> {
    /**
     * 成立时间
     */
    String establishDate;
    String fundName;
    /**
     * 基金名称
     */
    String fundNo;
    String id;
    String isDeputeManage;
    String lastQuarterUpdate;
    /**
     * 私募基金管理人名称
     */
    String managerName;
    String managerType;
    String managersInfo;
    /**
     * 托管人名称
     */
    String mandatorName;
    /**
     * 备案时间
     */
    String putOnRecordDate;
    String url;
    String workringState;

    @Override
    public void transform(InfodiscPofFundDetail infodiscPofFundDetail) {

    }
}
