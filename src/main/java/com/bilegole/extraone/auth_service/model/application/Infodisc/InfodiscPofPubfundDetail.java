package com.bilegole.extraone.auth_service.model.application.Infodisc;

import lombok.Data;

import java.util.Date;

/**
 * 基金公司私募投资基金
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/pubfund/index.html">基金公司私募投资基金JSON页</a>
 */
@Data
public class InfodiscPofPubfundDetail {
    /**
     * 产品编码
     */
    String productCode;
    /**
     * 产品名称
     */
    String productName;
    /**
     * 私募基金管理人名称
     */
    String managerName;
    /**
     * 托管人名称
     */
    String trusteeName;
    /**
     * 成立日期
     */
    Date establishDate;
    /**
     * 备案日期
     */
    String registrationDate;
}
