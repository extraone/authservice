package com.bilegole.extraone.auth_service.model.client.infodisc.RequestParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 私募基金管理人分类查询公示页面列表查询参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class InfodiscPofManagerRequestParam extends PageQueryParam {
    String autualControllers;
    String branchMgrs;
    String checkBeginDate;
    String checkEndDate;
    String creditInfoStatus;
    String establishDate;
    String foundBeginDate;
    String foundEndDate;
    String fundScale;
    String fundType;
    String hashFaith;
    String hasMatter;
    String keyword;
    String legalOffice;
    String memberType;
    String offiProvinceFsc;
    String officeCity;
    String officeProvince;
    String orgForm;
    String primaryInvestType;
    String regiProvinceFsc;
    String registerCity;
    String registerDate;
    String registerProvince;
    String warningTips;
}
