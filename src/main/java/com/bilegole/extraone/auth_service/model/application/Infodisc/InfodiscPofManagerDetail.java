package com.bilegole.extraone.auth_service.model.application.Infodisc;

import lombok.Data;

/**
 * 私募基金管理人
 * 来源有两个，私募基金管理人分类查询公示（JSON）
 * 私募基金管理人公示信息
 */
@Data
public class InfodiscPofManagerDetail {
    /**
     * 机构信息
     */
    OrgInfo orgInfo;
    MemberInfo memberInfo;
    LegalOpinions legalOpinions;
    Executives executives;
    AffiliatesInfo affiliatesInfo;
    Funders funders;
    ProductName productName;
}

class OrgInfo{

}

class MemberInfo{
    Boolean isMember;
}

class LegalOpinions{
    /**
     * 法律意见书状态
     */
    String stage;
    /**
     * 律师事务所名称
     */
    String lawFirm;
    /**
     * 律师姓名
     */
    String lawersName;
    /**
     * 实际控制人姓名
     */
    String ActualControllerName;
}

/**
 * 高管信息
 */
class Executives{
    /**
     * 职务
     */
    String posts;
    /**
     * 姓名
     */
    String name;
    /**
     * 是否有基金从业资格
     */
    Boolean qualified;
    /**
     * 获取从业资格方式
     */
    String wayTobeQualified;
    /**
     * 工作履历
     */
    String workHistory;
}

/**
 * 关联方信息
 */
class AffiliatesInfo{
    /**
     * 序号
     */
    String index;
    /**
     * 类型
     */
    String type;
    /**
     * 名称
     */
    String name;
    /**
     * 登记编号
     */
    String registryNo;
    /**
     * 组织机构代码
     */
    String orgCode;
}

class Funders{
    /**
     * 序号
     */
    Integer index;
    /**
     * 姓名
     */
    String name;
    /**
     * 认缴比例百分比
     */
    Integer SubscriptionRatio;
}

class ProductName {
    /**
     * 私募基金信息披露备份系统
     * 投资者查询账号开立率。
     *
     * 不是很懂这是什么。
     * example: 无在管理私募基金
     */
    String status;
}