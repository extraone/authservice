package com.bilegole.extraone.auth_service.model.client.infodisc.ReturnList;

import com.bilegole.extraone.auth_service.model.application.Infodisc.InfodiscPofAoinFundDetail;
import lombok.Data;

import java.util.Date;

/**
 * 证券公司直投基金页面列表返回值
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/aoin/product/index.html">样本页</a>
 */
@Data
public class InfodiscPofAoinProductList implements returnListItem<InfodiscPofAoinFundDetail> {
    /**
     * 直投子公司名称
     */
    String aoinName;
    String buyMoney;
    /**
     * 产品编码
     */
    String code;
    /**
     * 设立日期
     */
    Date createDate;
    String fundType;
    String id;
    String isDeputeManager;
    /**
     * 管理机构
     */
    String managerName;
    String mandatorName;
    /**
     * 产品名称
     */
    String name;
    String organizeType;
    String scope;
    String submitDate;
    String workingState;

    @Override
    public void transform(InfodiscPofAoinFundDetail infodiscPofAoinFundDetail) {

    }
}
