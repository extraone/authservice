package com.bilegole.extraone.auth_service.model.client.infodisc.ReturnList;

import com.bilegole.extraone.auth_service.model.application.Infodisc.InfodiscPofSubFundDetail;
import lombok.Data;

/**
 * 证券公司私募投资基金页面列表返回值
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/subfund/index.html">样本页</a>
 */
@Data
public class InfodiscPofSubfundList implements returnListItem<InfodiscPofSubFundDetail>{
    String castProduct;
    String delmark;
    /**
     * 成立日期
     */
    String foundDate;
    String fundStatus;
    String fundType;
    String id;
    /**
     * 私募基金管理人名称
     */
    String mgrName;
    String orgForm;
    /**
     * 产品编码
     */
    String productCode;
    String productId;
    /**
     * 产品名称
     */
    String productName;
    /**
     * 备案日期
     */
    String registeredDate;
    /**
     * 托管人名称
     */
    String trustee;
    String tuoGuan;
    String userTenantId;

    @Override
    public void transform(InfodiscPofSubFundDetail detail) {

    }
}
