package com.bilegole.extraone.auth_service.model.application.Infodisc;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 私募基金公示详情,来源于<br>
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/fund/index.html">私募基金公示JSON/私募基金管理人基金产品JSON</a><br>
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/fund/351000128801.html">私募基金公示信息HTML</a>
 */
@Data
public class InfodiscPofFundDetail {
    /**
     * 机构诚信信息
     */
    OrgHonestyInfo orgHonestyInfo;
    /**
     * 机构提示信息
     */
    OrgSpecialInfo orgSpecialInfo;
    /**
     * 基本信息
     */
    BasicInfo basicInfo;
    /**
     * 信息披露情况
     */
    InformationDisclosure informationDisclosure;
}

/**
 * 机构诚信信息
 */
class OrgHonestyInfo{
    List<String> honestInfos;
}

/**
 * 机构提示信息
 */
class OrgSpecialInfo{
    List<String> info;
}

/**
 * 基本信息
 */
class BasicInfo{
    /**
     * 编号
     */
    String fundName;
    /**
     * 基金编号
     */
    String fundNo;
    /**
     * 成立时间
     */
    Date establishDate;
    /**
     * 备案时间
     */
    Date RegistrationDate;
    /**
     * 基金类型
     */
    String fundType;
    /**
     * 币种
     */
    String currency;
    /**
     * 基金管理人名称
     */
    String fundManagerName;
    /**
     * 管理类型
     */
    String manageType;
    /**
     * 托管人名称
     */
    String trusteeName;
    /**
     * 运作状态
     */
    String Status;
    /**
     * 最后更新时间
     */
    Date lastUpdatedTime;
    /**
     * 基金业协会特别提示
     */
    String SpecialTips;
}

/**
 * 信息披露情况
 */
class InformationDisclosure{

}