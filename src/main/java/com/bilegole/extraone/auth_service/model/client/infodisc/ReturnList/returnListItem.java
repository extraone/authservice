package com.bilegole.extraone.auth_service.model.client.infodisc.ReturnList;

public interface returnListItem<T> {
    void transform(T t);
}
