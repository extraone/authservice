package com.bilegole.extraone.auth_service.model.client.infodisc.RequestParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 募基金公示列表查询请求参数
 * 又被称为私募基金管理人基金产品
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class InfodiscPofFundRequestParam extends PageQueryParam {
    String keyword;
    String managerType;
    String putOnRecordPhase;
    String workingState;
}
