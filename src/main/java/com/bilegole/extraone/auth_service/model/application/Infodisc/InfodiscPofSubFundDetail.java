package com.bilegole.extraone.auth_service.model.application.Infodisc;

import lombok.Data;

import java.util.Date;

/**
 * 证券公司私募投资基金核心对象<br>
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/subfund/index.html">证券公司私募投资基金JSON页</a><br>
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/pof/subfund/124100050017.html">证券公司私募投资基金HTML页</a>
 */
@Data
public class InfodiscPofSubFundDetail {
    /**
     * 产品名称
     */
    String productName;
    /**
     * 产品编码
     */
    String productCode;
    /**
     * 管理人名称
     */
    String managerName;
    /**
     * 成立日期
     */
    Date establishDate;
    /**
     * 备案日期
     */
    Date RegistrationDate;
    /**
     * 基金类型
     */
    String fundType;
    /**
     * 组织形式
     */
    String orgForm;
    /**
     * 运作状态
     */
    String Status;
    /**
     * 托管人名称
     */
    String trusteeName;
}
