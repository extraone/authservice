package com.bilegole.extraone.auth_service.model.application;

import lombok.Data;

@Data
public class User {
    /**
     * 用户Id,字符串
     * 暂定长度为10位。
     */
    String id;

    /**
     * 用户账号
     */
    String account;

    /**
     * 用户名
     */
    String name;

    /**
     * 用户邮箱
     */
    String mail;

    /**
     * 用户手机号
     */
    String phone;
}
