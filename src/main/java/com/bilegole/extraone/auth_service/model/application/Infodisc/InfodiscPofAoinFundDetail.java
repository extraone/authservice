package com.bilegole.extraone.auth_service.model.application.Infodisc;

import lombok.Data;

/**
 * 证券公司直投基金公示信息
 * <a href="https://gs.amac.org.cn/amac-infodisc/res/aoin/product/1522.html">样本页</a>
 */
@Data
public class InfodiscPofAoinFundDetail {
    /**
     * 产品名称
     */
    String name;
    /**
     * 产品编码
     */
    String productCode;
    /**
     * 直投子公司名称
     */
    String aoinCompanyName;
    /**
     * 管理机构名称
     */
    String managerOrgName;
    /**
     * 注册时间
     */
    String establishDate;
    /**
     * 备案时间
     */
    String registrationDate;
    /**
     * 基金类型
     */
    String fundType;
    /**
     * 组织形式
     */
    String orgForm;
    /**
     * 运作状态
     */
    String Status;
    /**
     * 是否托管
     */
    String hasTrustee;
    /**
     * 托管人名称
     */
    String trusteeName;
}
