package com.bilegole.extraone.auth_service.model.client.infodisc.RequestParam;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 证券公司私募投资基金公示参数
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class InfodiscPofSubfundRequestParam extends PageQueryParam {
    String foundDateFrom;
    String foundDateTo;
    String mgrName;
    String productCode;
    String productName;
}
