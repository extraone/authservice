package com.bilegole.extraone.auth_service.configuration;

import org.springframework.security.crypto.password.PasswordEncoder;

@Deprecated
public class PasswordDecoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        return rawPassword.toString();
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return rawPassword.toString().equals(encodedPassword);
    }
}
