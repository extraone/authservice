package com.bilegole.extraone.auth_service.controller;

import com.bilegole.extraone.auth_service.Service.UserService;
import com.bilegole.extraone.auth_service.model.application.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/getAll")
    List<User> getAllUser() {
        return userService.getAllUser();
    }

    @PostMapping("/update")
    Boolean updateUser(User user) {
        return userService.updateUser(user);
    }

    @PostMapping("/create")
    Boolean createUser(User user) {
        return userService.createUser(user);
    }
}
