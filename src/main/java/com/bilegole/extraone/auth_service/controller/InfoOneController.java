package com.bilegole.extraone.auth_service.controller;

import com.bilegole.extraone.auth_service.Service.InfoService;
import com.bilegole.extraone.auth_service.model.application.InfoOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class InfoOneController {
    @Autowired
    InfoService service;

    @RequestMapping(value = "/get/InfoOne",method = RequestMethod.GET)
    public List<InfoOne> getInfoOnes(){
        return service.getInfoOneList();
    }
}
