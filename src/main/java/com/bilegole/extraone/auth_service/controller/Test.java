package com.bilegole.extraone.auth_service.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yuang
 */
@RequestMapping("/test")
@RestController
public class Test {

    @RequestMapping(path = "/test1",method = RequestMethod.GET)
    public String test1(){
        return "test1";
    }
}
