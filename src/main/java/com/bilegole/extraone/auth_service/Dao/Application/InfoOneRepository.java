package com.bilegole.extraone.auth_service.Dao.Application;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InfoOneRepository extends JpaRepository<InfoOnePO, Long> {
}
