package com.bilegole.extraone.auth_service.Dao.Infodisc;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

/**
 * 私募基金公示
 */
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class InfodiscPofFundPo {
    /**
     * 私募基金编号
     */
    @Id
    String fundNo;
    /**
     * 基金名称
     */
    String fundName;
    /**
     * 私募基金管理人名称
     */
    String fundManagerName;
    /**
     * 托管人名称
     */
    String mandatorName;
    /**
     * 成立时间
     */
    Date establishDate;
    /**
     * 备案时间ß
     */
    Date registryDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        InfodiscPofFundPo that = (InfodiscPofFundPo) o;
        return fundNo != null && Objects.equals(fundNo, that.fundNo);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
