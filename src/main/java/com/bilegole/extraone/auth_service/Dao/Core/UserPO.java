package com.bilegole.extraone.auth_service.Dao.Core;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author yuang
 */
@Entity
@Getter
@Setter
public class UserPO {
    @Id
    String username;

    @Column
    String password;

    @Column
    Boolean enabled;
}
