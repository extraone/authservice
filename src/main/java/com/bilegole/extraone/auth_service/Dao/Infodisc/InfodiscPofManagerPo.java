package com.bilegole.extraone.auth_service.Dao.Infodisc;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

/**
 * 私募基金管理人分类查询公示
 */
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class InfodiscPofManagerPo {
    /**
     * 登记编号
     */
    @Id
    String registerNo;
    /**
     * 私募基金管理人名称
     */
    String managerName;
    /**
     * 法定代表人/执行事务合伙人(委派代表)姓名
     */
    String artificialPersonName;
    /**
     * 机构类型
     */
    String orgType;
    /**
     * 注册地
     */
    String registerAddress;
    /**
     * 办公地
     */
    String officeAddress;
    /**
     * 在管基金数量
     */
    String fundNumInManage;
    /**
     * 会员类型
     */
    String memberType;
    /**
     * 是否有提示信息
     */
    String hasSpecialTips;
    /**
     * 是否有诚信信息
     */
    String hasCreditTips;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        InfodiscPofManagerPo that = (InfodiscPofManagerPo) o;
        return registerNo != null && Objects.equals(registerNo, that.registerNo);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
