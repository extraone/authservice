package com.bilegole.extraone.auth_service.Dao.Infodisc;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class InfodiscPofPubfundPo {
    /**
     * 产品编码
     */
    @Id
    String productCode;
    /**
     * 产品名称
     */
    String productName;
    /**
     * 私募基金管理人名称
     */
    String mgrName;
    /**
     * 托管人名称
     */
    String mandatorName;
    /**
     * 成立日期
     */
    Date establishDate;
    /**
     * 备案日期
     */
    Date registryDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        InfodiscPofPubfundPo that = (InfodiscPofPubfundPo) o;
        return productCode != null && Objects.equals(productCode, that.productCode);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
