package com.bilegole.extraone.auth_service.Dao.Application;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class InfoOnePO {
    @Id
    private Long id;

    @Column
    private String Info1;
}
