package com.bilegole.extraone.auth_service.Dao.Infodisc;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

/**
 * 证券公司直投基金
 */
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class InfodiscAoinProductPo {
    /**
     * 产品编码
     */
    @Id
    String productCode;
    /**
     * 产品名称
     */
    String productName;
    /**
     * 直投子公司名称
     */
    String aoinCompanyName;
    /**
     * 管理机构
     */
    String managerCompanyName;
    /**
     * 设立日期
     */
    Date establishDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        InfodiscAoinProductPo that = (InfodiscAoinProductPo) o;
        return productCode != null && Objects.equals(productCode, that.productCode);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
