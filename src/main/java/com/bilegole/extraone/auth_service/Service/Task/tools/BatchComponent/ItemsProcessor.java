package com.bilegole.extraone.auth_service.Service.Task.tools.BatchComponent;

public interface ItemsProcessor<I,O> {
    O transform(I i);
}
