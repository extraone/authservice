package com.bilegole.extraone.auth_service.Service.Task.tools.Executor.Impl;

import com.bilegole.extraone.auth_service.Service.Task.tools.Executor.QueueTaskExecutor;
import com.bilegole.extraone.auth_service.Service.Task.tools.QueueTask;
import com.bilegole.extraone.auth_service.Service.Task.tools.Task;

/**
 * 单线程顺序便利执行器
 */
@SuppressWarnings("unused")
public class SimpleQueueTaskExecutorImpl implements QueueTaskExecutor {
    public void run(QueueTask task) {
        for (Task t : task.getQueue()) {
            t.execute();
        }
    }
}
