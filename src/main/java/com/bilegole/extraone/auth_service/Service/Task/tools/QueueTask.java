package com.bilegole.extraone.auth_service.Service.Task.tools;

import com.bilegole.extraone.auth_service.Service.Task.tools.Executor.QueueTaskExecutor;

import java.util.ArrayList;
import java.util.List;

/**
 * 队列任务
 */
public class QueueTask implements Task{
    protected List<Task> queue = new ArrayList<>();

    protected QueueTaskExecutor executor;

    /**
     * 本函数的功能就是吧一个任务添加到队列任务中。
     *
     * @param task 要添加的任务
     */
    @SuppressWarnings("unused")
    public QueueTask addTask(Task task) {
        queue.add(task);
        return this;
    }

    @SuppressWarnings("unused")
    public QueueTask addTasks(List<Task> tasks) {
        queue.addAll(tasks);
        return this;
    }

    public List<Task> getQueue() {
        return this.queue;
    }

    @SuppressWarnings("unused")
    public void execute(QueueTaskExecutor executor) {
        this.executor = executor;
        this.execute();
    }

    @Override
    public void execute() {
        this.executor.run(this);
    }
}
