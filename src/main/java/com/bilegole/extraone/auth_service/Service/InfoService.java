package com.bilegole.extraone.auth_service.Service;

import com.bilegole.extraone.auth_service.Dao.Application.InfoOnePO;
import com.bilegole.extraone.auth_service.Dao.Application.InfoOneRepository;
import com.bilegole.extraone.auth_service.model.application.InfoOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InfoService {
    @Autowired
    InfoOneRepository repository;

    public List<InfoOne> getInfoOneList(){
        List<InfoOnePO> res = repository.findAll();
        List<InfoOne> res_1 = new ArrayList<>();
        for(InfoOnePO po:res){
            res_1.add(new InfoOne(po));
        }
        return res_1;
    }
}
