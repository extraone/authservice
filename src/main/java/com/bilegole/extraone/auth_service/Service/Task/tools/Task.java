package com.bilegole.extraone.auth_service.Service.Task.tools;

public interface Task {
    /**
     * 对一个task类执行这个函数以启动任务
     */
    void execute();
}
