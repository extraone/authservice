package com.bilegole.extraone.auth_service.Service.impl;

import com.bilegole.extraone.auth_service.Service.UtilService;
import org.springframework.stereotype.Service;

import java.util.Random;

import static java.lang.Math.abs;

@Service
public class UtilServiceImpl implements UtilService {
    @Override
    public String getNewUserId() {
        Random random = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            builder.append(abs(random.nextInt()) % 10);
        }
        return builder.toString();
    }
}
