package com.bilegole.extraone.auth_service.Service.Task.tools.Executor;

import com.bilegole.extraone.auth_service.Service.Task.tools.SimpleBatchTask;

public interface SimpleBatchTaskExecutor<I, O> {
    void run(SimpleBatchTask<I, O> task);
}
