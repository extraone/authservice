package com.bilegole.extraone.auth_service.Service.client;

import com.bilegole.extraone.auth_service.model.client.infodisc.RequestParam.*;
import com.bilegole.extraone.auth_service.model.client.infodisc.ReturnList.*;

import java.util.List;

//@Deprecated
public interface InfodiscClient {

    /**
     * 私募基金管理人分类查询公示页面列表查询
     *
     * @return 私募基金管理人分类查询公示页面列表查询返回值
     */
    List<InfodiscPofManagerList> queryInfodiscPofManager(InfodiscPofManagerRequestParam param);

    /**
     * 私募基金公示列表查询（私募基金管理人基金产品）
     *
     * @return 私募基金公示列表查询查询返回值
     */
    List<InfodiscPofFundList> queryInfodiscPofFund(InfodiscPofFundRequestParam param);

    /**
     * 证券公司直投基金页面查询
     *
     * @return 证券公司直投基金页面查询返回值
     */
    List<InfodiscPofAoinProductList> queryInfodiscAoinProduct(InfodiscAoinProductRequestParam param);

    /**
     * 基金公司私募投资基金公示查询
     *
     * @return 私募基金公示返回值
     */
    List<InfodiscPofPubfundList> queryInfodiscPofPubFundList(InfodiscPofPubfundRequestParam param);

    /**
     * 证券公司私募投资基金公示
     *
     * @return 证券公司私募投资基金公示
     */
    List<InfodiscPofSubfundList> queryInfodiscPofSubFundList(InfodiscPofSubfundRequestParam param);

//    List<InfodiscPofManagerList> queryInfodiscPofManager(InfodiscPofManagerRequestParam param);
}
