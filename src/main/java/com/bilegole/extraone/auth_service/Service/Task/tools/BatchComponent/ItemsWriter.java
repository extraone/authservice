package com.bilegole.extraone.auth_service.Service.Task.tools.BatchComponent;

import java.util.List;

public interface ItemsWriter<O> {
    void write(List<O> items);
}
