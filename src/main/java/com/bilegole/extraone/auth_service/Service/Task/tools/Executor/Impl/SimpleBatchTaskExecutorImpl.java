package com.bilegole.extraone.auth_service.Service.Task.tools.Executor.Impl;

import com.bilegole.extraone.auth_service.Service.Task.tools.Executor.SimpleBatchTaskExecutor;
import com.bilegole.extraone.auth_service.Service.Task.tools.SimpleBatchTask;

import java.util.ArrayList;
import java.util.List;

/**
 * @param <I> 泛型，指从远程服务上直接拿到的对象
 * @param <O> 反省，指核心对象
 */
@SuppressWarnings("unused")
public class SimpleBatchTaskExecutorImpl<I, O> implements SimpleBatchTaskExecutor<I, O> {
    public void run(SimpleBatchTask<I, O> task) {
        while (!task.getReader().reachEnd()) {
            List<I> inputs = task.getReader().read();
            List<O> outputs = new ArrayList<>();
            for (I input : inputs) {
                outputs.add(task.getProcessor().transform(input));
            }
            task.getWriter().write(outputs);
        }

    }
}
