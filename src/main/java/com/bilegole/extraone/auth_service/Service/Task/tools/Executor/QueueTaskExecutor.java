package com.bilegole.extraone.auth_service.Service.Task.tools.Executor;

import com.bilegole.extraone.auth_service.Service.Task.tools.QueueTask;

public interface QueueTaskExecutor {
    void run(QueueTask task);
}
