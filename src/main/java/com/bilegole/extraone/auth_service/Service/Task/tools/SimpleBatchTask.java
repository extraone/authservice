package com.bilegole.extraone.auth_service.Service.Task.tools;

import com.bilegole.extraone.auth_service.Service.Task.tools.BatchComponent.ItemsProcessor;
import com.bilegole.extraone.auth_service.Service.Task.tools.BatchComponent.ItemsReader;
import com.bilegole.extraone.auth_service.Service.Task.tools.BatchComponent.ItemsWriter;
import com.bilegole.extraone.auth_service.Service.Task.tools.Executor.SimpleBatchTaskExecutor;
import lombok.Data;

@Data
public class SimpleBatchTask<I, O> implements Task {
    ItemsReader<I> reader;
    protected SimpleBatchTaskExecutor executor;
    ItemsWriter<O> writer;
    ItemsProcessor<I, O> processor;

    SimpleBatchTask(
            ItemsReader<I> reader,
            ItemsProcessor<I, O> processor,
            ItemsWriter<O> writer
    ) {
        this.reader = reader;
        this.processor = processor;
        this.writer = writer;
    }

    @SuppressWarnings("unused")
    public void execute(SimpleBatchTaskExecutor executor) {
        this.executor = executor;
        this.execute();
    }

    @Override
    public void execute() {
        this.executor.run(this);
    }


//
//    public static SimpleBatchTask<I,O> init(
//            ItemsReader<I> reader,
//            ItemsProcessor<I,O> processor,
//            ItemsWriter<O> writer
//    ){
//        return new SimpleBatchTask(reader,processor,writer);
//    }
}
