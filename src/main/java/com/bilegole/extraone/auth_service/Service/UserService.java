package com.bilegole.extraone.auth_service.Service;

import com.bilegole.extraone.auth_service.model.application.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    final
    UtilService utilService;

    public UserService(UtilService utilService) {
        this.utilService = utilService;
    }

    public List<User> getAllUser() {
        return new ArrayList<>();
    }

    public Boolean updateUser(User user) {
        return null;
    }

    /**
     * @param user 要被创建的用户.
     * @param id   被创建的用户应该使用哪个id.
     * @return 如果创建用户成功就返回True，否则返回False
     */
    public Boolean createUser(
            User user,
            String id
    ) {
        return null;
    }

    /**
     * @param user 要被创建的用户.
     * @return 如果创建用户成功就返回True，否则返回False
     */
    public Boolean createUser(User user) {
        return createUser(user, utilService.getNewUserId());
    }

    /**
     * @param userId 要删除的用户Id
     * @return 如果删除用户成功就返回True，否则返回False或者抛出异常。
     */
    public Boolean deleteUser(String userId) {
        return null;
    }
}
