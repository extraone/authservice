package com.bilegole.extraone.auth_service.Service.Task.tools;

/**
 * 任务执行器
 */
public interface TaskExecutor {
    void run(Task task);
}
